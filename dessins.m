clear;
close all;

Fe = 24000; % fréquence d'échantillonage (Hz)
Te = 1/Fe; % période d'échantillonage (s)
N = 10000; % nombre de bits envoyés
Rb = 3000; % débit binaire

M = 2^1; % signal BPSK
Ts = log2(M)/Rb; % période symbole
Ns = floor(Ts/Te);
T = (0:N*Ns/log2(M)-1) * Te; % échelle temporelle

alpha0 = 1;
alpha1 = 0.5;

n0 = Ns;

%% deux rectangles
figure;
LOS = ones(1, Ns);
multipath = ones(1, Ns)/2;
stairs(0:2*Ns-1, [LOS, multipath]);
ylim([0 1.1]);
xlim([0 2*Ns]);
title("h_e");

%% construction du signal avec les g
alpha0 = 1;
alpha1 = 0.5;
h = ones(1, Ns); % mise en forme: réponse impulsionnelle rectangulaire
h_c = [ alpha0 zeros(1, Ns-1) alpha1 ]; % propagation: sélectif
%h_c = [ 1 zeros(1, Ns-1) ]; % propagation: dirac
h_r = h; % réception: réponse impulsionnelle rectangulaire
g = conv(conv(h, h_c), h_r); % réponse impulsionnelle globale

bits = [ 1 1 1 0 0 1 0 ]; % bits envoyés
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis
X_r = filter(h_r, 1, X_c); % signal reçu, sans bruit

figure;
hold;
somme = zeros(1, length(g)+Ns*(length(bits)-1));
for i=0:length(bits)-1
   somme = somme + [zeros(1, i*Ns) g*sign(X_m(i+1)) zeros(1, (length(bits)-i-1)*Ns) ];
   plot( [zeros(1, i*Ns) g*sign(X_m(i+1)) zeros(1, (length(bits)-i-1)*Ns) ], '--');
   text(Ns*(i+1) + 0, X_m(i+1), num2str(bits(i+1)), 'Color', '#7E2F8E');
end
plot(somme, 'LineWidth', 2);
legend('g(t - 0*Ns)', 'g(t - 1*Ns)', 'g(t - 2*Ns)', 'g(t - 3*Ns)', 'g(t - 4*Ns)', 'g(t - 5*Ns)', 'g(t - 6*Ns)', 'somme');

%% construction de z

alpha0 = 1;
alpha1 = 0.5;
h = ones(1, Ns); % mise en forme: réponse impulsionnelle rectangulaire
h_c = [ alpha0 zeros(1, Ns-1) alpha1 ]; % propagation: sélectif
%h_c = [ 1 zeros(1, Ns-1) ]; % propagation: dirac
h_r = h; % réception: réponse impulsionnelle rectangulaire
g = conv(conv(h, h_c), h_r); % réponse impulsionnelle globale

bits = [ 1 0 0 0 0 0 0 ]; % bits envoyés
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis
X_r = filter(h_r, 1, X_c); % signal reçu, sans bruit

figure;
hold;
somme = zeros(1, length(g)+Ns*(length(bits)-1));
for i=0:length(bits)-1
   somme = somme + [zeros(1, i*Ns) g*sign(X_m(i+1)) zeros(1, (length(bits)-i-1)*Ns) ];
   plot( [zeros(1, i*Ns) g*sign(X_m(i+1)) zeros(1, (length(bits)-i-1)*Ns) ], '--');
   text(Ns*(i+1) + 0, X_m(i+1), num2str(bits(i+1)), 'Color', '#7E2F8E');
end
plot(somme);

%% quatres gaussiennes
T = -20:0.1:20;
sigma_w = sqrt(Ns);
g1 = gaussmf(T, [sigma_w -3*Ns/2]);
g2 = gaussmf(T, [sigma_w -Ns/2]);
g3 = gaussmf(T, [sigma_w Ns/2]);
g4 = gaussmf(T, [sigma_w 3*Ns/2]);

hold on;
plot(T, g1);
plot(T, g2);
plot(T, g3);
plot(T, g4);

legend("$$-\alpha_0-\alpha_1$$", "$$-\alpha_0+\alpha_1$$", "$$+\alpha_0-\alpha_1$$", "$$+\alpha_0+\alpha_1$$", 'interpreter','latex');

