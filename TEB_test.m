clear;
close all;

Fe = 24000; % fréquence d'échantillonage (Hz)
Te = 1/Fe; % période d'échantillonage (s)
N = 100000; % nombre de bits envoyés
Rb = 3000; % débit binaire
Fp = 2000; % fréquence porteuse

bits = [ 1 1 1 0 0 1 0 ];
% bits = randi([0, 1], 1, N); % bits envoyés

M = 2^1; % signal BPSK
Ts = log2(M)/Rb; % période symbole
Ns = floor(Ts/Te);
T = (0:N*Ns/log2(M)-1) * Te; % échelle temporelle

alpha0 = 1;
alpha1 = 0.5;

h = ones(1, Ns); % mise en forme: réponse impulsionnelle rectangulaire
h_c = [ alpha0 zeros(1, Ns-1) alpha1 ]; % propagation: sélectif
% h_c = [ 1 zeros(1, Ns-1) ]; % propagation: dirac
h_r = h; % réception: réponse impulsionnelle rectangulaire

n0 = Ns; % déterminé en traçant le diagramme de l'oeil ou g

%% tracé de g
g = conv(conv(h, h_c), h_r); % réponse impulsionnelle globale

figure;
plot(g);
title("Tracé de g");
ylabel("Amplitude");
axis square;

%% chaine de transmission
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis

%% tracé de x_e
figure;
stairs(X_c);
axis(axis*1.1);
title("Signal transmis");
ylabel("Amplitude");
for i=1:length(bits)
    index = n0/2 + (i-1)*Ns;
    text(index + 0.8, X_c(index), num2str(bits(i)), 'Color', '#7E2F8E');
end

%% diagramme de l'oeil
figure;
X_r = filter(h_r, 1, X_c); % signal reçu, sans bruit
oeil = reshape(X_r(Ns+1:end), Ns, length(X_r(Ns+1:end))/Ns); % permet de tracer le diagramme de l'oeil, on retrouve n0 = 8 
plot(oeil);
title("Diagramme de l'oeil");
ylabel("Amplitude");
axis square;

%% tracé de x_r
figure;
plot(X_r);
axis(axis*1.1);
title("Signal reçu");
ylabel("Amplitude");

%% réception
X_e = X_r( n0:Ns:length(bits)*Ns ); % échantillonage du signal reçu
recu = double( X_e > 0 ); % on récupère les bits, décision + "démapping"
erreur = mean((recu - bits).^2) % on calcule l'erreur

%% tracé des constellations
figure;
plot(complex(X_e(2:end)), "*");
hold;
plot(complex(X_m), ".", 'MarkerSize', 20);
axis(axis*1.1);
axis square;
title("Constellations");
legend("canal sélectif", "canal dirac");
xlabel("Re");
ylabel("Im");

%% tracé de TEB = f(E_b/N_0)
bits = randi([0, 1], 1, N); % bits envoyés
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis

P_x = mean(abs(X_c).^2); % puissance du signal en entrée de la reception
EbN0_db = linspace(0, 10, 200);
EbN0 = 10.^(EbN0_db./10);
TEBs = [];
for e=EbN0
    sigma2_x = P_x * Ns / (log2(M) * e); % calcul de sigma^2
    X_b = X_c + sqrt(sigma2_x) * randn(1, length(X_c)); % signal bruité
    X_r = filter(h_r, 1, X_b); % signal reçu
    
    X_e = X_r( n0:Ns:length(bits)*Ns/log2(M) ); % échantillonage du signal reçu
    
    recu = double( X_e > 0 ); % on récupère les bits, décision + "démapping"
    TEB = mean((recu - bits).^2); % on calcule l'erreur binaire
    
    TEBs = [ TEBs TEB ];
end

TEB_theorique = 1/2*qfunc(3/2*sqrt(4/5*EbN0)) + 1/2*qfunc(1/2*sqrt(4/5*EbN0));
% TEB_theorique = qfunc(sqrt(EbN0));

 close all;

figure;
semilogy(EbN0_db, TEBs, '+');
hold;
plot(EbN0_db, TEB_theorique);
title("TEB $$= f(E_b/N_0)$$", 'interpreter','latex');
xlabel("$$E_b/N_0$$ (dB)", 'interpreter','latex');
ylabel("TEB", 'interpreter','latex');
legend("TEB numérique", "TEB théorique (canal selectif)");

%% tracé de constellations bruitées

EbN0_db = linspace(0, 12, 4);
EbN0 = 10.^(EbN0_db./10);
for e=EbN0
    sigma2_x = P_x * Ns / (log2(M) * e); % calcul de sigma^2
    X_b = X_c + sqrt(sigma2_x) * randn(1, length(X_c)); % signal bruité
    X_r = filter(h_r, 1, X_b); % signal reçu
    
    X_e = X_r( n0:Ns:length(bits)*Ns/log2(M) ); % échantillonage du signal reçu
    
    recu = double( X_e > 0 ); % on récupère les bits, décision + "démapping"
    TEB = mean((recu - bits).^2); % on calcule l'erreur binaire

    close all;
    figure;
    
    sp1 = subplot(2, 1, 1);
    histogram(X_e, 100);
    xlim([-40 40]);
    xlabel("Re");
    title("Histogramme, $$E_b/N_0 =$$ " + num2str(10*log10(e)) +"dB", 'interpreter','latex');

    sp2 = subplot(2, 1, 2);
    plot(complex(X_e(2:10:end)), "*");
    hold;
    plot(complex(X_m), ".", 'MarkerSize', 20);
    xlim([-40 40]);
    title("Constellations, $$E_b/N_0 =$$ " + num2str(10*log10(e)) +"dB", 'interpreter','latex');
    legend("symboles bruités", "symboles théoriques");
    xlabel("Re");
    ylabel("Im");
    
    sp2.Position(4) = 0.1;
    sp1.Position(2) = 0.28;
    sp1.Position(4) = 0.68;

    pause(1);
    print(gcf,"histcons_selectif_" + num2str(10*log10(e)) + "db.png", '-dpng', '-r300');
    
end
