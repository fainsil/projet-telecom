clear;
close all;

Fe = 24000; % fréquence d'échantillonage (Hz)
Te = 1/Fe; % période d'échantillonage (s)
N = 50000; % nombre de bits envoyés
Rb = 3000; % débit binaire
Fp = 2000; % fréquence porteuse

% bits = [ 1 1 1 0 0 1 0 ];
bits = randi([0, 1], 1, N); % bits envoyés

M = 2^1; % signal BPSK
Ts = log2(M)/Rb; % période symbole
Ns = floor(Ts/Te);
T = (0:N*Ns/log2(M)-1) * Te; % échelle temporelle

alpha0 = 1;
alpha1 = 0.5;

h = ones(1, Ns); % mise en forme: réponse impulsionnelle rectangulaire
h_c = [ alpha0 zeros(1, Ns-1) alpha1 ]; % propagation: sélectif
% h_c = [ 1 zeros(1, Ns-1) ]; % propagation: dirac
h_r = h; % réception: réponse impulsionnelle rectangulaire

n0 = Ns; % déterminé en traçant le diagramme de l'oeil ou g

%% tracé de g
g = conv(conv(h, h_c), h_r); % réponse impulsionnelle globale

% figure;
% plot(g);
% title("Tracé de g");
% ylabel("Amplitude");

%% chaine de transmission
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis

%% tracé de X_c
% figure;
% stairs(X_c);
% axis(axis*1.1);

%% diagramme de l'oeil
figure;
X_r = filter(h_r, 1, X_c); % signal reçu, sans bruit
oeil = reshape(X_r(Ns+1:end), Ns, length(X_r(Ns+1:end))/Ns); % permet de tracer le diagramme de l'oeil, on retrouve n0 = 8 
plot(oeil);
title("Diagramme de l'oeil");
xlabel("Temps (s)");
ylabel("Amplitude");

%% tracé de TEB = f(E_b/N_0)
P_x = mean(abs(X_c).^2);
EbN0_db = linspace(0, 10, 200);
EbN0 = 10.^(EbN0_db./10);
TEBs = [];
for e=EbN0
    
    sigma2_x = P_x * Ns / (2 * log2(M) * e); % calcul de sigma^2
    X_b = X_c + sqrt(sigma2_x) * randn(1, length(X_c)); % signal bruité
    X_r = filter(h_r, 1, X_b); % signal reçu
    
    X_e = X_r( n0:Ns:length(bits)*Ns/log2(M) ); % échantillonage du signal reçu
    
    recu = double( X_e > 0 ); % on récupère les bits, décision + "démapping"
    TEB = mean((recu - bits).^2); % on calcule l'erreur binaire
    
    TEBs = [ TEBs TEB ];

end

bidule = sqrt ( EbN0 / 2 );
TEB_theorique = 1/2*qfunc(3 * bidule) + 1/2*qfunc(bidule);

figure;
semilogy(EbN0_db, TEBs, '+');
hold;
plot(EbN0_db, TEB_theorique);
title("TEB $$= f(E_b/N_0)$$", 'interpreter','latex');
xlabel("$$E_b/N_0$$ (dB)", 'interpreter','latex');
ylabel("TEB", 'interpreter','latex');
