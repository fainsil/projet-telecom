clear;
close all;

Fe = 24000; % fréquence d'échantillonage (Hz)
Te = 1/Fe; % période d'échantillonage (s)
N = 50000; % nombre de bits envoyés (bit)
Rb = 3000; % débit binaire (bit/s)

% bits = [ 1 1 1 0 0 1 0 ];

M = 2^1; % signal BPSK
Ts = log2(M)/Rb; % période symbole
Ns = floor(Ts/Te);
T = (0:N*Ns/log2(M)-1) * Te; % échelle temporelle

h = ones(1, Ns); % mise en forme: réponse impulsionnelle rectangulaire
h_c = [ 1 zeros(1, Ns-1) 0.5 ]; % propagation: sélectif
h_r = h; % réception: réponse impulsionnelle rectangulaire

n0 = Ns; % déterminé en traçant le diagramme de l'oeil ou g

%% création de Z et C

ordre = 10;
K = ordre;

bits = [ 1 zeros(1, ordre-1) ]; % dirac
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis
X_r = filter(h_r, 1, X_c); % signal reçu, sans bruit
X_e = X_r( n0:Ns:length(bits)*Ns ); % échantillonage du signal reçu

Z = zeros(K, ordre);
for i=1:ordre
    Z(i:end, i) = X_e(1:K-i+1);
end

Y_0 = [ 1 zeros(1, ordre-1) ]';
C = Z \ Y_0;

%% tracé de X_c
% figure;
% plot(X_e);
% axis(axis*1.1);
% title("non égalisé");

%% tracé de X_egal

% X_egal = filter(C, 1, X_e);

% figure;
% plot(X_egal);
% axis(axis*1.1);
% title("égalisé");

%% tracé de g et g_eg

g = conv(conv(h, h_c), h_r);
g_eg = conv(g, C);

figure;
plot(abs(fft(g, 4096)));
figure;
plot(abs(fft(g_eg, 4096)));

%% tracé de g et g_eg

% figure;
% plot(g);
% figure;
% plot(g_eg);

%% tracé de TEB = f(E_b/N_0)

bits = randi([0, 1], 1, N); % bits envoyés
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis

P_x = mean(abs(X_c).^2);
EbN0_db = linspace(0, 10, 200);
EbN0 = 10.^(EbN0_db./10);
TEBs = [];
for e=EbN0
    
    sigma2_x = P_x * Ns / (2 * log2(M) * e); % calcul de sigma^2
    X_b = X_c + sqrt(sigma2_x) * randn(1, length(X_c)); % signal bruité
    
    X_eg = filter(C, 1, X_b); % égalisation ZFE
    X_r = filter(h_r, 1, X_eg); % signal reçu
    
    X_e = X_r( n0:Ns:length(bits)*Ns/log2(M) ); % échantillonage du signal reçu
    
    recu = double( X_e > 0 ); % on récupère les bits, décision + "démapping"
    TEB = mean((recu - bits).^2); % on calcule l'erreur binaire
    
    TEBs = [ TEBs TEB ];

end

bidule = sqrt ( EbN0 / 2 );
TEB_theorique = 1/2*qfunc(3 * bidule) + 1/2*qfunc(bidule);

figure;
semilogy(EbN0_db, TEBs, '+');
hold;
plot(EbN0_db, TEB_theorique);
title("TEB $$= f(E_b/N_0)$$", 'interpreter','latex');
xlabel("$$E_b/N_0$$ (dB)", 'interpreter','latex');
ylabel("TEB", 'interpreter','latex');

%% tracé des constellations

figure;
plot(complex(X_m), '.', 'MarkerSize', 25);
hold;
plot(complex(X_e), '+');
plot(complex(X_eg), '*');
axis([-2 2 -1 1]);
legend("symboles", "non égalisé", "égalisé");