clear;
close all;

Fe = 24000; % fréquence d'échantillonage (Hz)
Te = 1/Fe; % période d'échantillonage (s)
N = 10; % nombre de bits envoyés
Rb = 3000; % débit binaire

M = 2^1; % signal BPSK
Ts = log2(M)/Rb; % période symbole
Ns = floor(Ts/Te);
T = (0:N*Ns/log2(M)-1) * Te; % échelle temporelle

alpha0 = 1;
alpha1 = 0.5;

h = ones(1, Ns); % mise en forme: réponse impulsionnelle rectangulaire
h_c = [ alpha0 zeros(1, Ns-1) alpha1 ]; % propagation: sélectif
h_r = h; % réception: réponse impulsionnelle rectangulaire

n0 = Ns; % déterminé en traçant le diagramme de l'oeil ou g

%% création de Z et C

ordre = 10;
K = ordre;

bits = [ 1 zeros(1, ordre-1) ]; % dirac
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis
X_r = filter(h_r, 1, X_c); % signal reçu, sans bruit
X_e = X_r( n0 : Ns : length(bits)*Ns ); % échantillonage du signal reçu

Z = zeros(K, ordre);
for i=1:ordre
    Z(i:end, i) = X_e(1:K-i+1);
end

Y_0 = [ 1 zeros(1, ordre-1) ]';
C = Z \ Y_0;

X_eg = filter(C, 1, X_e);

g = conv(conv(h, h_c), h_r); % chaine globale

% figure;
% plot(g);
% hold;
% plot(g_eg);

% figure;
% plot(abs(fft(C, 2^10)));
% title("C");
% figure;
% plot(abs(fft(g, 2^10)));
% title("g");
% figure;
% plot(abs(fft(g, 2^10).*fft(C', 2^10)));
% title("g \times C");

%% tracé d'une réponse impulsionnelle

bits = [ 1 zeros(1, N-1) ];
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis
X_r = filter(h_r, 1, X_c); % signal reçu, sans bruit
X_e = X_r( n0 : Ns : length(bits)*Ns ); % échantillonage du signal reçu
X_eg = filter(C, 1, X_e); % égaliseur ZFE

figure;
stairs(X_m);
title("X_m");
axis([1 10 -1.1 1.1]);

figure;
plot(X_e);
title("X_e");
saveaxis = axis;
figure;

plot(X_eg);
title("X_{eg}");
axis(saveaxis);