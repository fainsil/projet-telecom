clear;
close all;

Fe = 24000; % fréquence d'échantillonage (Hz)
Te = 1/Fe; % période d'échantillonage (s)
N = 10000; % nombre de bits envoyés
Rb = 3000; % débit binaire
Fp = 2000; % fréquence porteuse

bits = [ 1 1 1 0 0 1 0 ];
% bits = randi([0, 1], 1, N); % bits envoyés

M = 2^1; % signal BPSK
Ts = log2(M)/Rb; % période symbole
Ns = floor(Ts/Te);
T = (0:N*Ns/log2(M)-1) * Te; % échelle temporelle

alpha0 = 1;
alpha1 = 0.5;

h = ones(1, Ns); % mise en forme: réponse impulsionnelle rectangulaire
h_c = [ alpha0 zeros(1, Ns-1) alpha1 ]; % propagation: sélectif
% h_c = [ 1 zeros(1, Ns-1) ]; % propagation: dirac
h_r = h; % réception: réponse impulsionnelle rectangulaire

n0 = Ns; % déterminé en traçant le diagramme de l'oeil ou g

%% tracé de g
g = conv(conv(h, h_c), h_r); % réponse impulsionnelle globale

figure;
plot(g);
title("Tracé de g");
ylabel("Amplitude");
axis square;

%% chaine de transmission
X_m = 2 * bits - 1; % mapping binaire à moyenne nulle
X_k = kron(X_m, [1 zeros(1, Ns-1)]); % Suréchantillonnage
X_f = filter(h, 1, X_k); % signal émis
X_c = filter(h_c, 1, X_f); % signal transmis

%% tracé de x_e
figure;
stairs(X_c);
axis(axis*1.1);
title("Signal transmis");
ylabel("Amplitude");
for i=1:length(bits)
    index = n0/2 + (i-1)*Ns;
    text(index + 0.8, X_c(index), num2str(bits(i)), 'Color', '#7E2F8E');
end

%% diagramme de l'oeil
figure;
X_r = filter(h_r, 1, X_c); % signal reçu, sans bruit
oeil = reshape(X_r(Ns+1:end), Ns, length(X_r(Ns+1:end))/Ns); % permet de tracer le diagramme de l'oeil, on retrouve n0 = 8 
plot(oeil);
title("Diagramme de l'oeil");
ylabel("Amplitude");
axis square;

%% tracé de x_r
figure;
plot(X_r);
axis(axis*1.1);
title("Signal reçu");
ylabel("Amplitude");

%% réception
X_e = X_r( n0:Ns:length(bits)*Ns ); % échantillonage du signal reçu
recu = double( X_e > 0 ); % on récupère les bits, décision + "démapping"
erreur = mean((recu - bits).^2) % on calcule l'erreur

%% tracé des constellations
figure;
plot(complex(X_m), ".", 'MarkerSize', 20);
hold;
plot(complex(X_e(2:end)), "*");
axis(axis*1.1);
title("Constellations");
legend("canal dirac", "canal sélectif");
xlabel("Re");
ylabel("Im");
